<?php
class PO_Efinance_Model_Notification extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('efinance/notification');
    }
}