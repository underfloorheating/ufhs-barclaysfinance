<?php

class PO_Efinance_Model_Resource_Notification extends Mage_Core_Model_Resource_Db_Abstract{
    protected function _construct()
    {
        $this->_init('efinance/notification', 'id');
    }
}