<?php

class PO_Efinance_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup {

    public function startSetup()
    {
        $this->_conn->multi_query("SET SQL_MODE='';
            SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
            SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
            ");
        return $this;
    }

    public function endSetup()
    {
        $this->_conn->multi_query("
            SET SQL_MODE=IFNULL(@OLD_SQL_MODE,'');
            SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS,0);
            ");
        return $this;
    }

}