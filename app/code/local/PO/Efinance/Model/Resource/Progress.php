<?php

class PO_Efinance_Model_Resource_Progress extends Mage_Core_Model_Resource_Db_Abstract{
    protected function _construct()
    {
        $this->_init('efinance/progress', 'id');
        $this->_isPkAutoIncrement = false;
    }
}