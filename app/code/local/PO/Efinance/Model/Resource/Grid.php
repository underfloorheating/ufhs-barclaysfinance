<?php

class PO_Efinance_Model_Resource_Grid extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('efinance/efinance');
    }
}