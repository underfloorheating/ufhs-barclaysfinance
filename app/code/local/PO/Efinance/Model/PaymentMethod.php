<?php

    class PO_Efinance_Model_PaymentMethod extends Mage_Payment_Model_Method_Abstract
    {

        /**
         * IMPORTANT! Adds a prefix to internal customer ref number, helps with duplication
         */
        private $CustomerRefPrefix = 300;

        /**
         * unique internal payment method identifier
         */
        protected $_code = 'efinance';

        /**
         * Location for frontend template
         */
        protected $_formBlockType = 'efinance/bpf';

        /**
         * Is this payment method a gateway (online auth/charge) ?
         */
        protected $_isGateway = false;

        /**
         * Can authorize online?
         */
        protected $_canAuthorize = true;

        /**
         * Can capture funds online?
         */
        protected $_canCapture = true;

        /**
         * Can capture partial amounts online?
         */
        protected $_canCapturePartial = false;

        /**
         * Can refund online?
         */
        protected $_canRefund = false;

        /**
         * Can void transactions online?
         */
        protected $_canVoid = true;

        /**
         * Can use this payment method in administration panel?
         */
        protected $_canUseInternal = true;

        /**
         * Can show this payment method as an option on checkout payment page?
         */
        protected $_canUseCheckout = true;

        /**
         * Is this payment method suitable for multi-shipping checkout?
         */
        protected $_canUseForMultishipping = true;

        /**
         * Can save credit card information for future processing?
         */
        protected $_canSaveCc = false;

        /**
         * BPF application token received from application
         */
        private $applicationToken;

        /**
         * BPF proposal ID received back from application
         */
        private $ProposalID;

        /**
         * Construct
         */
        public function __construct ()
        {
            if (Mage::getStoreConfig('payment/po_efinance/api_username') == '' || Mage::getStoreConfig('payment/po_efinance/api_password') == '' || Mage::getStoreConfig('payment/po_efinance/bpf_enabled') == 0) {
                $this->_canUseCheckout = false;
            }
        }

        /**
         * Returns redirect of URL
         */
        public function getOrderPlaceRedirectUrl ()
        {
            $this->bpfApply();

            return Mage::getStoreConfig('payment/po_efinance/api_redirecturl') . '?token=' . $this->applicationToken;
        }

        /**
         * Authorizes Payment option, Return true to
         */
        public function authorize (Varien_Object $payment, $amount)
        {
            return false;
        }

        /**
         * Function to apply for BPF finance, sets Application Token Globally
         */
        private function bpfApply ()
        {

            $apiUsername = Mage::getStoreConfig('payment/po_efinance/api_username');
            $apiPassword = Mage::getStoreConfig('payment/po_efinance/api_password');
            $tokenUrl = Mage::getStoreConfig('payment/po_efinance/api_redirecturl');
            $barclaysPostUrl = Mage::getStoreConfig('payment/po_efinance/api_submiturl');

            // Start new BPF API class
            $Bpf = new Bpf_BpfAppShort($apiUsername, $apiPassword, $tokenUrl, $barclaysPostUrl);

            $goodsArray = array();

            // Checkout information
            $customerBillingInfo = Mage::getSingleton('checkout/session')->getQuote()->getBillingAddress()->getData();

            // Checkout information
            $customerShippingInfo = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getData();

            // Get Total Price including shipping
            $sTotalPrice = Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal();

            // Get magnetos reserved id for this order
            $sReservedOrderId = Mage::getSingleton('checkout/session')->getQuote()->getReservedOrderId();

            // Get products from cart and loop through to set in correct format for API class
            $cart = Mage::getModel('checkout/cart')->getQuote();
            foreach ($cart->getAllItems() as $item) {
                $_product = Mage::getSingleton('catalog/product')->load($item->getProduct()->getId());
                $bpfAssetType = $_product->getResource()->getAttribute('bpf_asset_type')->getFrontend()->getValue($_product);
                $assetType = $bpfAssetType != 'No' ? $bpfAssetType : 'FH1';
                $goodsArray[$assetType][] = array(
                    "Description" => $item->getProduct()->getName(),
                    "Quantity"    => $item->getQty()
                );
            }

            $Bpf->aOrderInfo = array(
                "CustomerReference" => $this->CustomerRefPrefix . $customerBillingInfo['quote_id'],
                "CashPrice"         => $sTotalPrice,
                "goods"             => $goodsArray,
            );

            $Bpf->aCustomerInfo = array(
                "customer_info" => array(
                    "Forename"     => $customerBillingInfo['firstname'],
                    "Initial"      => $customerBillingInfo['middlename'],
                    "Surname"      => $customerBillingInfo['lastname'],
                    "EmailAddress" => $customerBillingInfo['email'],
                    "HomePhoneNumber" => $customerBillingInfo['telephone']
                ),
                "billing_address_info"  => array(
                    "Street"   => $customerBillingInfo['street'],
                    "District" => $customerBillingInfo['region'],
                    "Town"     => $customerBillingInfo['city'],
                    "County"   => $customerBillingInfo['region'],
                    "Postcode" => str_replace(' ', '', $customerBillingInfo['postcode'])
                ),
                "shipping_address_info"  => array(
                    "Street"   => $customerShippingInfo['street'],
                    "District" => $customerShippingInfo['region'],
                    "Town"     => $customerShippingInfo['city'],
                    "County"   => $customerShippingInfo['region'],
                    "Postcode" => str_replace(' ', '', $customerShippingInfo['postcode'])
                )
            );

            // Submit Application and set token
            $application = $Bpf->submitNewApplicationShort($Bpf->aOrderInfo['CustomerReference']);
            $this->applicationToken = $application['applicationToken'];
            $this->ProposalID = $application['ProposalID'];

            // If errors are set
            if (!empty($application['errors']))
                Mage::log($application['errors'], null, "BPF_errors.log");

            // API returns 0000 token if application isn't successful
            if ($application['applicationToken'] == '00000000-0000-0000-0000-000000000000')
                $Bpf->aOrderInfo['CustomerReference'] = '0';

            // Set Insert Array for DB
            $aInsertArray = array(
                "id"                 => $sReservedOrderId,
                "customer_reference" => $Bpf->aOrderInfo['CustomerReference'],
                "cash_price"         => $Bpf->aOrderInfo['CashPrice'],
                "goods_information"  => json_encode($goodsArray),
                "application_token"  => $application['applicationToken'],
                "proposal_id"        => $application['ProposalID'],
            );

            // Init efinace model
            $model = Mage::getModel('efinance/efinance');
            $model->setData($aInsertArray);
            $model->save();

            /* Init Efinance Helper class and Update the proposal */
            $helper = Mage::helper('efinance');
            $helper ->UpdateProposal($sReservedOrderId);

        }

    }

?>
