<?php
class PO_Efinance_Model_Observer {

    public function CompleteOrder($observer) {

        $order      = $observer->getOrder();
        $Order      = Mage::getModel('sales/order')->load($order->getId())->getData();
        $collection = Mage::getModel('efinance/efinance')->getCollection()->addFieldToFilter('id', $Order['increment_id'])->getFirstItem();

        if($collection->getId()){

            if($order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE){
                $helper = Mage::helper( 'efinance' );
                $helper->SubmitNotificationBatch($order->getId());
            }
        }
    }

}
?>