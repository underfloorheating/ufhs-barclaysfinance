<?php
class PO_Efinance_Helper_Data extends Mage_Core_Helper_Abstract
{

    /** Remove Application Checklist Conditions */
    public function removeApplicationConditions($ProposalId)
    {
        $checklistModelCollection   = Mage::getModel('efinance/checklist')->getCollection();
        $PreviousChecklists         = $checklistModelCollection->addFieldToFilter('proposal_id', $ProposalId);

        foreach ($PreviousChecklists as $ccitem) {
            $ccitem->delete();
        }
    }

    /** Log Application checklist conditions */
    public function updateApplicationConditions($aChecklistConditions, $ProposalId)
    {
        /* Get Checklist Model */
        $checklistModel             = Mage::getModel('efinance/checklist');

        foreach ($aChecklistConditions as $condition) {
            $sType              = $condition['@attributes']['Type'];
            $sSatisfied         = $condition['Satisfied'];
            $sChecklistStatus   = $condition['Status'];
            $sSatisfiedOn       = $condition['SatisfiedOn'];

            $checklistModel->unsetData();
            $checklistModel->addData(array (
                "proposal_id"       => $ProposalId,
                "type"              => $sType,
                "satisfied"         => $sSatisfied,
                "checklist_status"  => $sChecklistStatus,
                "satisfied_on"      => $sSatisfiedOn,
            ))->save();
        }
    }

    public function logApplicationProgress($ProposalId, $Status)
    {
        $model = Mage::getModel('efinance/efinance')->load($ProposalId, 'proposal_id');

        /* Check if proposal exists */
        if($id = $model->getId()) {

            // Get previous application status from model
            $sPreviousStatus = $model->getApplicationStatus();

            $model->addData(array (
                "application_status"    => $Status));

            // Check if application status has changed
            if ( $sPreviousStatus != $Status ) {

                $modelProgress = Mage::getModel('efinance/progress');
                $modelProgress->setData(array(
                    "order_id"          => $id,
                    "previous_status"   => $sPreviousStatus,
                    "current_status"    => $Status,
                    "timestamp"         => time()
                ));

                $modelProgress->save();
                $model->setId($id)->save();
            }
        }
    }

    public function UpdateProposal($orderId){

        $aBpfData           = $model = Mage::getModel('efinance/efinance')->load($orderId)->getData();
        $ProposalId         = $aBpfData['proposal_id'];
        $sCustomerReference = $aBpfData['customer_reference'];

        $apiUsername        = Mage::getStoreConfig('payment/po_efinance/api_username');
        $apiPassword        = Mage::getStoreConfig('payment/po_efinance/api_password');
        $tokenUrl           = Mage::getStoreConfig('payment/po_efinance/api_redirecturl');
        $barclaysPostUrl    = Mage::getStoreConfig('payment/po_efinance/api_submiturl');

        $Bpf            = new Bpf_BpfProposal($apiUsername, $apiPassword, $tokenUrl, $barclaysPostUrl);
        $application    = $Bpf->getProposalEnquiry($sCustomerReference, $ProposalId);

        $aChecklistConditions = $application['ChecklistConditions'];
        $Status               = $application['status'];

        /* Log Application Change */
        $this->logApplicationProgress($ProposalId, $Status);

        /* Delete Previous records of Proposal Checklist Conditions */
        $this->removeApplicationConditions($ProposalId);

        /* Update new Checklist Conditions */
        $this->updateApplicationConditions($aChecklistConditions, $ProposalId);

        $order = Mage::getModel('efinance/efinance')->load($ProposalId, 'proposal_id');
        $order->addData(array(
            "application_status"    => $Status,
            "updated"               => time()
        ))->save();
    }

    public function SubmitNotificationBatch($orderId)
    {
        $Order              = Mage::getModel( 'sales/order' )->load($orderId)->getData();
        $aBpfData           = Mage::getModel('efinance/efinance')->load($Order['increment_id'])->getData();
        $clientReference    = $aBpfData['customer_reference'];
        $BalanceToFinance   = $aBpfData['cash_price'];
        $proposalid         = $aBpfData['proposal_id'];

        $order = Mage::getModel('efinance/notification');
        $order->addData(
            array(
                "proposal_id"       => $proposalid,
                "client_reference"  => $clientReference,
                "balance_to_finance"=> $BalanceToFinance
            )
        );
        $order->save();
        $sBatchId = $order->getId();

        $apiUsername        = Mage::getStoreConfig('payment/po_efinance/api_username');
        $apiPassword        = Mage::getStoreConfig('payment/po_efinance/api_password');
        $tokenUrl           = Mage::getStoreConfig('payment/po_efinance/api_redirecturl');
        $barclaysPostUrl    = Mage::getStoreConfig('payment/po_efinance/api_submiturl');

        // Start new BPF API class
        $Bpf            = new Bpf_BpfSubmitBatch($apiUsername, $apiPassword, $tokenUrl, $barclaysPostUrl);
        $BatchRequest   = $Bpf->submitNotificationBatch($clientReference, $proposalid, $BalanceToFinance, $sBatchId);

        $order->unsetData();
        $order->load($sBatchId);
        $order->addData(array("is_error"    => $BatchRequest['IsError']));
        $order->addData(array("errors"      => json_encode($this->getArrayValue($BatchRequest, 'Errors/ErrorDetails/ErrorDetail/@attributes', [])))); 
        $order->save();

        return $BatchRequest;
    }

    public function CancelApplication($orderId)
    {
        $Order      = Mage::getModel('sales/order')->load($orderId)->getData();
        $aBpfData   = $model = Mage::getModel('efinance/efinance')->load($Order['increment_id'])->getData();

        $apiUsername        = Mage::getStoreConfig('payment/po_efinance/api_username');
        $apiPassword        = Mage::getStoreConfig('payment/po_efinance/api_password');
        $tokenUrl           = Mage::getStoreConfig('payment/po_efinance/api_redirecturl');
        $barclaysPostUrl    = Mage::getStoreConfig('payment/po_efinance/api_submiturl');

        $Bpf = new Bpf_BpfCancelApp($apiUsername, $apiPassword, $tokenUrl, $barclaysPostUrl);

        $Bpf->aOrderInfo = array(
            "CustomerReference" => $aBpfData['customer_reference'],
        );

        $Bpf->aAgreementInfo = array(
            "OriginalLoanAdvance"       => $aBpfData['cash_price'],
            "CancellationAmount"        => $aBpfData['cash_price'],
            "NewLoanAdvance"            => 0,
            "CancellationType"          => "Full",
            "ClientRequestReference"    => $aBpfData['customer_reference'],
            "ProposalId"                => $aBpfData['proposal_id']
        );

        $Bpf->aCustomerInfo = array(
            "Forename"      => $Order['customer_firstname'],
            "Surname"       => $Order['customer_lastname'],
            "EmailAddress"  => $Order['customer_email'],
        );

        $application = $Bpf->cancelApplication($Bpf->aOrderInfo['CustomerReference']);
        Mage::log($application, null, "BPF.log");
    }

    /**
     * Updates the status of an order based on the passed string
     * @param  STRING $proposalId The proposal ID stored in the bpf table
     * @param  STRING $status     The status from Barclays
     */
    public function updateOrderStatus($proposalId, $status)
    {
        $model = Mage::getModel('efinance/efinance')->load($proposalId, 'proposal_id');

        /* Check if proposal exists */
        if($id = $model->getId()) {
            if($order = Mage::getModel('sales/order')->load($id, 'increment_id')) {
                // If we have a Ready to Deliver status, update the order so it can be pulled through by OrderWise
                switch ($status) {
                    case 'Ready To Deliver':
                        $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
                        $order->save();
                        break;
                    case 'Declined':
                    case 'Not Taken Up':
                        $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true);
                        $order->save();
                        break;
                }
            }
        }
    }

    /**
     * Recursively check for the existence of a key in a mulitdimensional array 
     * and return the value if it's found, else return the $failureResponse value
     * 
     * @param  Array   $array           The multidimensional array we are searching
     * @param  String  $keyPath         A string representing the keys of the MD array
     * @param  boolean $failureResponse What we want the function to return if no key is found
     * @return Mixed                    The failureResponse value passed to the function, defaults to FALSE
     */    
    public function getArrayValue($array, $keyPath, $failureResponse = false)
    {
        if(!is_array($array)) return false;

        $keys = explode("/", $keyPath);

        for($i=0; $i<count($keys); $i++) {
            if(isset($array[$keys[$i]])) {
                if(count($keys)==1) {
                    return $array[$keys[$i]];
                }
                $kPath = implode('/', array_slice($keys, $i + 1));
                return $this->getArrayValue($array[$keys[$i]], $kPath, $failureResponse);
            }
            else {
                break;
            }
        }
        return $failureResponse;
    }

}
	 