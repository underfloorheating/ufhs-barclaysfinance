<?php

/**
 * Changes the data type for the bpf.cash_price column to decimal
 */
$installer = $this;

$installer->startSetup();
$installer->run("
    ALTER TABLE `{$installer->getTable('efinance/efinance')}`
    CHANGE cash_price cash_price decimal(8,2) not null;
");
$installer->endSetup();