<?php

$table = "core_config_data";
$coreConfigTableName = Mage::getSingleton("core/resource")->getTableName($table);

$installer = $this;

$installer->startSetup();
$installer->run("
    CREATE TABLE IF NOT EXISTS `{$installer->getTable('efinance/efinance')}` (
      `id` int(11) NOT NULL,
      `customer_reference` int(11) NOT NULL,
      `cash_price` int(11) NOT NULL,
      `goods_information` text CHARACTER SET utf8 NOT NULL,
      `application_token` varchar(255) NOT NULL,
      `proposal_id` int(11) NOT NULL,
      `application_status` varchar(255) NOT NULL,
      `updated` int(11) NOT NULL,
      `timestamp` int(11) NOT NULL,
      PRIMARY KEY  (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    INSERT INTO `{$coreConfigTableName}` (`scope`, `scope_id`, `path`, `value`) VALUES ('default', '0', 'payment/po_efinance/bpf_enabled', '0');
    INSERT INTO `{$coreConfigTableName}` (`scope`, `scope_id`, `path`, `value`) VALUES ('default', '0', 'payment/po_efinance/api_redirecturl', 'https://iceua1v3.barclays-loans.com/eFinance/eFinance.aspx');
    INSERT INTO `{$coreConfigTableName}` (`scope`, `scope_id`, `path`, `value`) VALUES ('default', '0', 'payment/po_efinance/api_submiturl', 'https://iceua1.barclays-loans.com/WebService/BpfWebService/eFinance.asmx');
    INSERT INTO `{$coreConfigTableName}` (`scope`, `scope_id`, `path`, `value`) VALUES ('default', '0', 'payment/po_efinance/api_username', 'webcreation_icet1');
    INSERT INTO `{$coreConfigTableName}` (`scope`, `scope_id`, `path`, `value`) VALUES ('default', '0', 'payment/po_efinance/api_password', 'Passw0rd');
    INSERT INTO `{$coreConfigTableName}` (`scope`, `scope_id`, `path`, `value`) VALUES ('default', '0', 'payment/po_efinance/customer_ref_pre', 'Passw0rd');

CREATE TABLE IF NOT EXISTS `bpf_checklist_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proposal_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `satisfied` varchar(6) NOT NULL,
  `checklist_status` varchar(255) NOT NULL,
  `satisfied_on` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `id` (`proposal_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
CREATE TABLE IF NOT EXISTS `bpf_applications_progress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `previous_status` varchar(255) NOT NULL,
  `current_status` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
CREATE TABLE IF NOT EXISTS `bpf_notification_batch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proposal_id` int(11) NOT NULL,
  `client_reference` int(11) NOT NULL,
  `balance_to_finance` int(11) NOT NULL,
  `is_error` varchar(6) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
");

$statusTable        = $installer->getTable('sales/order_status');
$statusStateTable   = $installer->getTable('sales/order_status_state');
$statusLabelTable   = $installer->getTable('sales/order_status_label');

$dataStatus = array(array('status' => 'barclays_processing', 'label' => 'Processing Barclays'));
$dataState  = array(array('status' => 'barclays_processing', 'state' => 'barclays_processing', 'is_default' => 1));

$installer->getConnection()->insertArray($statusTable, array('status', 'label'), $dataStatus);
$installer->getConnection()->insertArray($statusStateTable, array('status', 'state', 'is_default'), $dataState);

$installer->endSetup();
