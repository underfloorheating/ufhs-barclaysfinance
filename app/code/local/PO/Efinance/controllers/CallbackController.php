<?php

class PO_Efinance_CallbackController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {

        $xml = file_get_contents('php://input');

        if(!empty($xml)){

            $Bpf = new Bpf_BpfCallback();
            $Bpf ->parseCallback($xml);

            $aResponseArray         = $Bpf->returnArray;
            $Status                 = $aResponseArray['status'];
            $ProposalId             = $aResponseArray['ProposalID'];
            $aChecklistConditions   = $aResponseArray['ChecklistConditions'];

            $helper = Mage::helper('efinance');

            /* Log Application Change */
            $helper->logApplicationProgress($ProposalId, $Status);

            /* Delete Previous records of Proposal Checklist Conditions */
            $helper->removeApplicationConditions($ProposalId);

            /* Update new Checklist Conditions */
            $helper->updateApplicationConditions($aChecklistConditions, $ProposalId);

            /* Update order status */
            $helper->updateOrderStatus($ProposalId, $Status);

            $responseXML = $Bpf->returnCallbackXML();

            $this->getResponse()->setHeader('Content-type', 'text/xml');
            $this->getResponse()->setBody($responseXML);
        }

        Mage::log("A callback was made from ".$_SERVER['REMOTE_ADDR']." at ".date("F j, Y, g:i a"), null, "BPF_callback.log");

    }
}
