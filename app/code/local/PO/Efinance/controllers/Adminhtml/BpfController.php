<?php
class PO_Efinance_Adminhtml_BpfController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_initAction()->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/po_efinance')
            ->_title($this->__('Sales'))->_title($this->__('Barclays Partner Finance'))
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Barclays Partner Finance'), $this->__('Barclays Partner Finance'));

        return $this;
    }

    public function introducerresponseAction()
    {
        $this->_initAction()->renderLayout();
    }

    /**
     * Action to update proposal based on 'Order Id' being passed as GET statement
     */
    public function updatepropAction()
    {
        if($orderId = $this->getRequest()->getParam('order_id')){

            $Order = Mage::getModel('sales/order')->load($orderId)->getData();

            /* Init Efinance Helper class and Update the proposal */
            $helper = Mage::helper( 'efinance' );
            $helper ->UpdateProposal( $Order['increment_id'] );

            /* Redirect to product page with message */
            Mage::getSingleton( 'adminhtml/session' )
                ->addSuccess( $this->__( "This Barclay's application has been updated successfully." ) );

            $this->_redirect( '*/sales_order/view', array( 'order_id' => $orderId ) );
        }
    }

    /**
     * Action to submit Notification Batch (Delivery Note) for the given 'Order Id' passed as GET statement
     */
    public function submitbatchAction()
    {
        if($orderId = $this->getRequest()->getParam('order_id')){

            /* Init Efinance Helper class and Update the proposal */
            $helper         = Mage::helper( 'efinance' );
            $BatchRequest   = $helper->SubmitNotificationBatch($orderId);

            /* Store Mage session */
            $MageSession = Mage::getSingleton( 'adminhtml/session' );

            /* Check for to see if delivery note has been excepted */
            if( $BatchRequest['NotificationsAccepted'] >= 1 ) {
                $MageSession->addSuccess( $this->__( "The Notification Batch has been submitted successfully." ) );
            } else {
                $MageSession->addError( $this->__( "There was a problem when submitting the notification batch for this application." ) );
            }

            /* Redirect back to Order Page */
            $this->_redirect( '*/sales_order/view', array( 'order_id' => $orderId ) );
        }
    }

    /**
     * Action to cancel Application
     */
    public function cancelapplicationAction()
    {
        if($orderId = $this->getRequest()->getParam('order_id')){

            /* Init Efinance Helper class and Update the proposal */
            $helper = Mage::helper( 'efinance' );
            $helper ->CancelApplication($orderId);
        }
    }

    /**
     * Action to Submit introducer note to underwriters when requested
     */
    public function introducersubmitAction()
    {
        $post = $this->getRequest()->getPost();

        try {
            if (empty($post)) {
                Mage::throwException($this->__('Invalid form data.'));
            }

            $sApplicationId         = $post['introducer_response_form']['order_id'];
            $sIntroducerResponse    = $post['introducer_response_form']['introducer_response'];

            $order = Mage::getModel('efinance/efinance')->load($sApplicationId, 'id');
            $order->addData(
                array(
                    "application_status"    => "Introducer Submitted",
                    "introducer_response"   => $sIntroducerResponse,
                    "updated"               => time()
                )
            )->save();

            // Update application Log
            $modelProgress = Mage::getModel('efinance/progress');
            $modelProgress->setData(array(
                "order_id"          => $sApplicationId,
                "previous_status"   => "Introducer Pend",
                "current_status"    => "Introducer Submitted",
                "timestamp"         => time()
            ))->save();

            $message = $this->__('Your response has been submitted successfully.');
            Mage::getSingleton('adminhtml/session')->addSuccess($message);
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        $order  = Mage::getModel('sales/order')->load($sApplicationId, 'increment_id');
        $id     = $order->getId();

        $this->_redirect('*/sales_order/view', array('order_id' => $id));
    }

}