<?php

class PO_Efinance_Block_Payment extends Mage_Payment_Block_Info
{

    protected function _construct()
    {
        parent::_construct();

        if($orderId        = $this->getRequest()->getParam('order_id')){
            $Order          = Mage::getModel('sales/order')->load($orderId);
            $sPaymentMethod = $Order->getPayment()->getMethodInstance()->getTitle();

            if($sPaymentMethod == 'Barclays Partner Finance'){
                $this->setTemplate('bpf/payment/bpf.phtml');
            }else{
                $this->setTemplate('payment/info/default.phtml');
            }

        }else{
            $this->setTemplate('payment/info/default.phtml');
        }


    }

}