<?php
class PO_Efinance_Block_Adminhtml_Bpf_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     */
    public function __construct()
    {
        $this->_blockGroup = 'efinance';
        $this->_controller = 'adminhtml_bpf';

        parent::__construct();

        $this->_updateButton('save', 'label', $this->__('Save Application'));
    }

    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('efinance_bpf')->getId()) {
            return $this->__('Bpf Application');
        }
        else {
            return $this->__('New Application');
        }
    }
}