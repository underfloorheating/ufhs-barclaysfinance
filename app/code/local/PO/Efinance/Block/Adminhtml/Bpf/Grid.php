<?php

class PO_Efinance_Block_Adminhtml_Bpf_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('updated');
        $this->setId('po_efinance_bpf_grid');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);

        $model                  = Mage::getModel('efinance/efinance');
        $collection             = $model->getCollection()->addFieldToFilter('application_status', 'Introducer Pend');
        $sAttentionNeededCount  = $collection->getSize();

        if($sAttentionNeededCount > 0){

            $message = $this->__("Note: $sAttentionNeededCount Proposal(s) need your attention.");
            Mage::getSingleton('adminhtml/session')->addWarning($message);

        }
    }

    protected function _getCollectionClass()
    {
        return 'efinance/grid';
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id',
            array(
                'header'=> $this->__('Order Id'),
                'align' =>'right',
                'width' => '50px',
                'index' => 'id'
            )
        );

        $this->addColumn('customer_reference',
            array(
                'header'=> $this->__('Customer Reference'),
                'index' => 'customer_reference'
            )
        );

        $this->addColumn('application_token',
            array(
                'header'=> $this->__('Application Token'),
                'index' => 'application_token'
            )
        );

        $this->addColumn('proposal_id',
            array(
                'header'=> $this->__('Proposal Id'),
                'index' => 'proposal_id'
            )
        );

        $this->addColumn('application_status',
            array(
                'header'=> $this->__('Application Status'),
                'index' => 'application_status'
            )
        );

        $this->addColumn('updated',
            array(
                'header'=> $this->__('Updated'),
                'index' => 'updated',
                'type'  => 'datetime'
            )
        );

        $this->addColumn('created',
            array(
                'header'=> $this->__('Created'),
                'index' => 'timestamp',
                'type'  => 'datetime'
            )
        );

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {

            $order  = Mage::getModel('sales/order')->load($row->getId(), 'increment_id');
            $id     = $order->getId();

            return $this->getUrl('*/sales_order/view', array('order_id' => $id));
        }
        return false;
    }
}