<?php
class PO_Efinance_Block_Adminhtml_Bpf extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'efinance';
        $this->_controller = 'adminhtml_bpf';
        $this->_headerText = $this->__('Barclays Partner Finance Applications');

        parent::__construct();
        $this->_removeButton('add');
    }
}