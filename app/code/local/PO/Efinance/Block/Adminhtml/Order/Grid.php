<?php

class PO_Efinance_Block_Adminhtml_Order_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('timestamp');
        $this->setId('po_efinance_order_grid');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _getCollectionClass()
    {
        return 'efinance/progress_grid';
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass())->addFieldToFilter('order_id', $this->increment_id);
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        /**$this->addColumn('previous_status',
            array(
                'header'=> $this->__("Previous Status"),
                'index' => 'previous_status'
            )
        );*/

        $this->addColumn('current_status',
            array(
                'header'=> $this->__("Status"),
                'index' => 'current_status'
            )
        );

        $this->addColumn('timestamp',
            array(
                'header'=> $this->__("Updated"),
                'index' => 'timestamp',
                'type'  => 'datetime'
            )
        );

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        #return $this->getUrl('*/bpf/introducerresponse', array('order_id' => $this->increment_id));
    }

}