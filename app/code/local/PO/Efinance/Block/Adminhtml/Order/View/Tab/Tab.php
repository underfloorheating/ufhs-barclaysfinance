<?php
class PO_Efinance_Block_Adminhtml_Order_View_Tab_Tab
    extends Mage_Adminhtml_Block_Template
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('bpf/order/view/tab/tab.phtml');
    }

    public function getTabLabel() {
        return $this->__('Bpf Application Progress');
    }

    public function getTabTitle() {
        return $this->__('Barclays Partner Finance');
    }

    public function canShowTab() {
        return true;
    }

    public function isHidden() {
        return false;
    }

    public function getOrder(){
        return Mage::registry('current_order');
    }
}
?>