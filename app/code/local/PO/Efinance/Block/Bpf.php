<?php
class PO_Efinance_Block_Bpf extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('bpf/PaymentSelect.phtml');
    }
}