<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 20/05/14
 * Time: 17:29
 */

class Bpf_BpfSubmitBatch extends Bpf_BpfClass{

    /**
     * @param $username
     * @param $password
     * @param $tokenUrl
     * @param $barclaysPostUrl
     */
    public function __construct($username, $password, $tokenUrl, $barclaysPostUrl)
    {

        $this->apiLoginName     = $username;
        $this->apiPassword      = $password;
        $this->tokenUrl         = $tokenUrl;
        $this->barclaysPostUrl  = $barclaysPostUrl;

    }

    /**
     * @param $clientReference
     * @param $proposalid
     * @return mixed
     */
    public function submitNotificationBatch($clientReference, $proposalid, $BalanceToFinance, $sBatchId)
    {
        $this->soapActionUrl    = 'http://www.barclays-partnerfinance.com/SubmitNotificationBatch';
        $this->ClientReference  = $clientReference;
        $this->ProposalID       = $proposalid;
        $this->xmlRequest       = $this->generateSubmitNotificationBatchXML($BalanceToFinance, $sBatchId);

        $request = $this->send();

        Mage::log("NotificatioBatch UFHS --> BPF : " . $this->xmlRequest, null, 'BPF_callback.log');
        Mage::log("NotificatioBatch BPF --> UFHS : " . $request, null, 'BPF_callback.log');

        $this->parseReturn($request);

        return $this->returnArray;
    }

    /**
     * @return mixed
     */
    private function generateSubmitNotificationBatchXML($BalanceToFinance, $sBatchId)
    {
        $xml = new SimpleXMLElement('<Envelope/>');

        $soapBody                = $xml->addChild('Body');
        $NewApplication          = $soapBody->addChild('SubmitNotificationBatch');
        $NewApplication          ->addAttribute("xmlns", "http://www.barclays-partnerfinance.com/");
        $NewApplicationDataShort = $NewApplication->addChild('notificationData');

        $UserCredentials = $NewApplicationDataShort->addChild('UserCredentials');
        $UserCredentials->addChild('LoginName', $this->apiLoginName);
        $UserCredentials->addChild('Password', $this->apiPassword);

        $NotificationBatch  = $NewApplicationDataShort->addChild('NotificationBatch');
        $NotificationBatch  ->addAttribute('BatchReference', $sBatchId);
        $Notification       = $NotificationBatch->addChild('Notification');
        $Notification       ->addAttribute('ProposalID', $this->ProposalID);
        $Notification       ->addAttribute('AgreementNumber', '');
        $Notification       ->addChild('ClientReference', $this->ClientReference);
        $Notification       ->addChild('BalanceToFinance', $BalanceToFinance);

        $sml = $this->soapyfyXML($xml->asXML());

        return $sml;
    }

    /**
     * @param $xml_string
     * @return mixed
     */
    public function parseReturn($xml_string)
    {
        $helper = Mage::helper('efinance');

        $xml = simplexml_load_string($xml_string);
        $xml->registerXPathNamespace('envoy', 'http://www.barclays-partnerfinance.com/');

        $applicationResponse = $xml->xpath('//envoy:SubmitNotificationBatchResponse');

        // Application response is returned as a SOAP object
        $responseArray = json_decode(json_encode($applicationResponse), 1);

        // Master return array set
        $this->returnArray['IsError']               = $helper->getArrayValue($responseArray[0], 'SubmitNotificationBatchResult/Errors/@attributes/IsError');
        $this->returnArray['Errors']                = $helper->getArrayValue($responseArray[0], 'SubmitNotificationBatchResult/Errors');
        $this->returnArray['NotificationRejections']= $helper->getArrayValue($responseArray[0], 'SubmitNotificationBatchResult/NotificationRejections');
        $this->returnArray['NotificationsAccepted'] = $helper->getArrayValue($responseArray[0], 'SubmitNotificationBatchResult/NumberOfNotificationsAccepted');

        return $responseArray;

    }

} 