<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 19/05/14
 * Time: 12:10
 */

require_once('ExSimpleXMLElement.php');

/*ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);*/

class Bpf_BpfClass {

    public $xmlRequest;
    public $header;
    public $soapActionUrl;
    public $ClientReference;
    public $apiLoginName;
    public $apiPassword;

    public $tokenUrl;
    public $barclaysPostUrl;

    public $aOrderInfo;
    public $aCustomerInfo;
    public $aAgreementInfo;
    public $applicationToken;
    public $ProposalID;
    public $applicationErrors;
    public $returnArray;

    private function set_header(){
        $this->header = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: \"".$this->soapActionUrl."\"",
            "Content-length: ".strlen($this->xmlRequest)
        );
    }

    protected function send(){

        $this->set_header();
        $soapCURL = curl_init();
        curl_setopt($soapCURL, CURLOPT_URL, $this->barclaysPostUrl );
        curl_setopt($soapCURL, CURLOPT_CONNECTTIMEOUT, 100);
        curl_setopt($soapCURL, CURLOPT_TIMEOUT,        1000);
        curl_setopt($soapCURL, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($soapCURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soapCURL, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soapCURL, CURLOPT_POST,           true );
        curl_setopt($soapCURL, CURLOPT_POSTFIELDS,     $this->xmlRequest);
        curl_setopt($soapCURL, CURLOPT_HTTPHEADER,     $this->header);
        curl_setopt($soapCURL, CURLOPT_HEADER,         false);

        $result = curl_exec($soapCURL);
        if($result === false) {
            $err = 'Curl error: ' . curl_error($soapCURL);
            $result = $err;
        }
        curl_close($soapCURL);

        return $result;
    }

    protected function soapyfyXML($xml)
    {

        $sml = str_replace("<Envelope>", '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">', $xml);
        $sml = str_replace("</Envelope>", '</soap12:Envelope>', $sml);
        $sml = str_replace("<Body>", '<soap12:Body>', $sml);
        $sml = str_replace("</Body>", '</soap12:Body>', $sml);
        $sml = str_replace('<?xml version="1.0"?>', '<?xml version="1.0" encoding="utf-8"?>', $sml);
        $sml = str_replace('<SubmitNewApplicationShort>', '<SubmitNewApplicationShort xmlns="http://www.barclays-partnerfinance.com/">', $sml);
        $sml = str_replace('<ProposalEnquiry>', '<ProposalEnquiry xmlns="http://www.barclays-partnerfinance.com/">', $sml);

        return $sml;

    }

    protected function xml2array ( $xmlObject, $out = array () )
    {
        foreach ( (array) $xmlObject as $index => $node )
            $out[$index] = ( is_object ( $node ) ) ? $this->xml2array ( $node ) : $node;

        return $out;
    }

} 