<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 20/05/14
 * Time: 16:09
 */

class Bpf_BpfAppShort extends Bpf_BpfClass{

    /**
     * @param $username
     * @param $password
     * @param $tokenUrl
     * @param $barclaysPostUrl
     */
    public function __construct($username, $password, $tokenUrl, $barclaysPostUrl)
    {

        $this->apiLoginName     = $username;
        $this->apiPassword      = $password;
        $this->tokenUrl         = $tokenUrl;
        $this->barclaysPostUrl  = $barclaysPostUrl;

    }

    /**
     * @param $clientReference
     * @return mixed#
     */
    public function submitNewApplicationShort($clientReference)
    {
        // Check for customer reference number and return custom error if not set
        if($clientReference == ''){
            $this->setCustomerReferenceError();
            return $this->returnArray;
        }

        $this->soapActionUrl    = 'http://www.barclays-partnerfinance.com/SubmitNewApplicationShort';
        $this->ClientReference  = $clientReference;
        $this->xmlRequest       = $this->generateApplicationShortXML();

        $Request = $this->send();

        Mage::log("ApplicationShort UFHS --> BPF : " . $this->xmlRequest, null, 'BPF_callback.log');
        Mage::log("NotificatioBatch BPF --> UFHS : " . $Request, null, 'BPF_callback.log');
        
        $this->parseApplicationXML($Request);

        return $this->returnArray;
    }

    /**
     * @return mixed
     */
    private function generateApplicationShortXML()
    {
        $xml = new ExSimpleXMLElement('<Envelope/>');

        $aOrderInfo     = $this->aOrderInfo;
        $aCustomerInfo  = $this->aCustomerInfo;

        $soapBody                = $xml->addChild('Body');
        $NewApplication          = $soapBody->addChild('SubmitNewApplicationShort');
        $NewApplicationDataShort = $NewApplication->addChild('newApplicationDataShort');

        $UserCredentials = $NewApplicationDataShort->addChild('UserCredentials');
        $UserCredentials->addChild('LoginName', $this->apiLoginName);
        $UserCredentials->addChild('Password', $this->apiPassword);

        $ProposalShort = $NewApplicationDataShort->addChild('ProposalShort');
        $ProposalShort->addChild('ClientReference', $this->ClientReference);
        $ProposalShort->addChild('CashPrice', $aOrderInfo['CashPrice']);

        if(isset($aOrderInfo['goods'])){
            foreach($aOrderInfo['goods'] as $assetType => $goodsArray){
                foreach($goodsArray as $values) {
                    $Goods = $NewApplicationDataShort->addChild('Goods');
                    $Goods->addAttribute('Type', $assetType);
                    foreach($values as $key2 => $value2){
                        $Goods->addChildCData($key2, $value2);
                    }
                }
            }
        }

        // Loop through customer info (title, forename, initial etc...)
        $Customer = $NewApplicationDataShort->addChild('Customer');
        if(isset($aCustomerInfo['customer_info'])){
            foreach($aCustomerInfo['customer_info'] as $key => $value){
                $Customer->addChild($key, $value);
            }
        }

        // Loop through address info (street, postcode, county etc...)
        $Address = $NewApplicationDataShort->addChild('Address');
        if(isset($aCustomerInfo['billing_address_info'])){
            foreach($aCustomerInfo['billing_address_info'] as $key => $value){
                $Address->addChild($key, $value);
            }
        }

        // Loop through address info (street, postcode, county etc...)
        $Delivery = $NewApplicationDataShort->addChild('Delivery');
        if(isset($aCustomerInfo['shipping_address_info'])){
            $deliveryAddress = $Delivery->addChild('DeliveryAddress');
            foreach($aCustomerInfo['shipping_address_info'] as $key => $value){
                $deliveryAddress->addChild($key, $value);
            }
        }

        $sml = $this->soapyfyXML($xml->asXML());

        return $sml;
    }

    /**
     * @param $xml_string
     */
    public function parseApplicationXML($xml_string)
    {
        $xml = simplexml_load_string($xml_string);
        $xml->registerXPathNamespace('envoy', 'http://www.barclays-partnerfinance.com/');

        $applicationResponse = $xml->xpath('//envoy:SubmitNewApplicationShortResult');

        // Application response is returned as a SOAP object
        $responseArray = json_decode(json_encode($applicationResponse), 1);

        $aParsedErrors = $this->parseErrors($responseArray);

        // Master return array set
        $this->returnArray['errors']            = $aParsedErrors;
        $this->returnArray['applicationToken']  = $responseArray[0]['Token'];
        $this->returnArray['ProposalID']        = $responseArray[0]['ProposalID'];
    }

    /**
     * @param $responseArray
     * @return array
     */
    private function parseErrors($responseArray)
    {
        $aParsedErrors = array();
        if(isset($responseArray[0]['Errors'])){
            foreach($responseArray[0]['Errors']['ErrorDetails']['ErrorDetail'] as $key => $errors)
                // If/else to check for error codes, BPF returns different arrays depending on error
                if(isset($errors['@attributes'])){
                    // Don't display a particular error code (not needed)
                    if($errors['@attributes']['Code'] != 1)
                        $aParsedErrors[$key] = $errors['@attributes'];
                }elseif(isset($errors['Code'])){
                    $aParsedErrors[$key] = $errors;
                }
        }
        return $aParsedErrors;
    }

    /**
     *
     */
    private function setCustomerReferenceError()
    {
        $this->returnArray['errors'] = array(
            "errors" => array(
                "Message" => "Customer Reference Number Not set.",
                "Code"  => "999"
            )
        );
    }

    /**
     *
     */
    private function setGoodsError()
    {
        $this->returnArray['errors'] = array(
            "errors" => array(
                "Message" => "Goods information is incorrect.",
                "Code"  => "999"
            )
        );
    }

} 