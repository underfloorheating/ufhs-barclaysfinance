<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 20/05/14
 * Time: 17:29
 */

class Bpf_BpfProposal extends Bpf_BpfClass{

    /**
     * @param $username
     * @param $password
     * @param $tokenUrl
     * @param $barclaysPostUrl
     */
    public function __construct($username, $password, $tokenUrl, $barclaysPostUrl)
    {

        $this->apiLoginName     = $username;
        $this->apiPassword      = $password;
        $this->tokenUrl         = $tokenUrl;
        $this->barclaysPostUrl  = $barclaysPostUrl;

    }

    /**
     * @param $clientReference
     * @param $proposalid
     * @return mixed
     */
    public function getProposalEnquiry($clientReference, $proposalid)
    {
        $this->soapActionUrl    = 'http://www.barclays-partnerfinance.com/ProposalEnquiry';
        $this->ClientReference  = $clientReference;
        $this->ProposalID       = $proposalid;
        $this->xmlRequest       = $this->generateProposalEnquiryXML();

        $request = $this->send();

        Mage::log("ProposalEnquiry UFHS --> BPF : " . $this->xmlRequest, null, 'BPF_callback.log');
        Mage::log("ProposalEnquiry BPF --> UFHS : " . $request, null, 'BPF_callback.log');

        $this->parseReturn($request);

        return $this->returnArray;
    }

    /**
     * @return mixed
     */
    private function generateProposalEnquiryXML()
    {
        $xml = new SimpleXMLElement('<Envelope/>');

        $soapBody                = $xml->addChild('Body');
        $NewApplication          = $soapBody->addChild('ProposalEnquiry');
        $NewApplicationDataShort = $NewApplication->addChild('proposalEnquiryData');

        $UserCredentials = $NewApplicationDataShort->addChild('UserCredentials');
        $UserCredentials->addChild('LoginName', $this->apiLoginName);
        $UserCredentials->addChild('Password', $this->apiPassword);

        $NewApplicationDataShort->addChild('ProposalID', $this->ProposalID);
        $NewApplicationDataShort->addChild('ClientReference', $this->ClientReference);

        $sml = $this->soapyfyXML($xml->asXML());

        return $sml;
    }

    /**
     * @param $xml_string
     * @return mixed
     */
    public function parseReturn($xml_string)
    {
        $helper = Mage::helper('efinance');

        $xml = simplexml_load_string($xml_string);
        $xml->registerXPathNamespace('envoy', 'http://www.barclays-partnerfinance.com/');

        $applicationResponse = $xml->xpath('//envoy:ProposalEnquiryResponse');

        // Application response is returned as a SOAP object
        $responseArray = json_decode(json_encode($applicationResponse), 1);

        if(isset($responseArray[0]['ProposalEnquiryResult']['ProposalId'])){
            $this->returnArray['ProposalID'] = $responseArray[0]['ProposalEnquiryResult']['ProposalId'];
        }else{
            $this->returnArray['ProposalID'] = $responseArray[0]['ProposalEnquiryResult']['ProposalID'];
        }

        // Master return array set
        $this->returnArray['ClientReference']       = $responseArray[0]['ProposalEnquiryResult']['ClientReference'];
        $this->returnArray['status']                = $responseArray[0]['ProposalEnquiryResult']['ProposalStatus']['Status'];
        $this->returnArray['ChecklistConditions']   = $helper->getArrayValue($responseArray[0], 'ProposalEnquiryResult/ChecklistConditions/ChecklistCondition', []);

        return $responseArray;

    }

} 