<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 20/05/14
 * Time: 17:29
 */

class Bpf_BpfCallback extends Bpf_BpfClass{

    /**
     * @param $xml_string
     * @return mixed
     */
    public function parseCallback($xml_string)
    {
        $helper = Mage::helper('efinance');

        $xml = simplexml_load_string($xml_string);
        $xml->registerXPathNamespace('envoy', 'http://www.barclays-partnerfinance.com/');

        $callbackResponse = $xml->xpath('//envoy:Callback');

        // Application response is returned as a SOAP object
        $responseArray = json_decode(json_encode($callbackResponse), 1);

        // Master return array set
        $this->returnArray['ProposalID']            = $responseArray[0]['ProposalID'];
        $this->returnArray['status']                = $responseArray[0]['ProposalStatus']['Status'];
        $this->returnArray['IsParked']              = $responseArray[0]['ProposalStatus']['@attributes']['IsParked'];
        $this->returnArray['ChecklistConditions']   = $helper->getArrayValue($responseArray[0], 'ChecklistConditions/ChecklistCondition', []);

        Mage::log("Callback BPF --> UFHS : " . $xml_string, null, 'BPF_callback.log');

        return $responseArray;

    }

    public function returnCallbackXML()
    {

        $xml = new SimpleXMLElement('<Envelope/>');

        $soapBody   = $xml->addChild('Body');
        $CancelApp  = $soapBody->addChild('CallbackResponse');
        $CancelApp  ->addAttribute('xmlns', 'http://www.barclays-partnerfinance.com/');
        $CancelApp  ->addChild('CallbackResult');

        $sml = $this->soapyfCallbackyXML($xml->asXML());

        Mage::log("Callback UFHS --> BPF : " . $sml, null, 'BPF_callback.log');

        return $sml;

    }

    private function soapyfCallbackyXML($xml)
    {

        $sml = str_replace("<Envelope>", '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">', $xml);
        $sml = str_replace("</Envelope>", '</soap:Envelope>', $sml);
        $sml = str_replace("<Body>", '<soap:Body>', $sml);
        $sml = str_replace("</Body>", '</soap:Body>', $sml);
        $sml = str_replace('<?xml version="1.0"?>', '<?xml version="1.0" encoding="utf-8"?>', $sml);
        $sml = str_replace('<SubmitNewApplicationShort>', '<SubmitNewApplicationShort xmlns="http://www.barclays-partnerfinance.com/">', $sml);
        $sml = str_replace('<ProposalEnquiry>', '<ProposalEnquiry xmlns="http://www.barclays-partnerfinance.com/">', $sml);

        return $sml;

    }

} 