<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 20/05/14
 * Time: 16:09
 */

class Bpf_BpfCancelApp extends Bpf_BpfClass{

    /**
     * @param $username
     * @param $password
     * @param $tokenUrl
     * @param $barclaysPostUrl
     */
    public function __construct($username, $password, $tokenUrl, $barclaysPostUrl)
    {

        $this->apiLoginName     = $username;
        $this->apiPassword      = $password;
        $this->tokenUrl         = $tokenUrl;
        $this->barclaysPostUrl  = $barclaysPostUrl;

    }

    /**
     * @param $clientReference
     * @return mixed#
     */
    public function cancelApplication($clientReference)
    {
        // Check for customer reference number and return custom error if not set
        if($clientReference == ''){
            $this->setCustomerReferenceError();
            return $this->returnArray;
        }

        $this->soapActionUrl    = 'http://www.barclays-partnerfinance.com/CancelAgreement';
        $this->ClientReference  = $clientReference;
        $this->xmlRequest       = $this->generateCancelAppXML();

        $Request = $this->send();
        $this->parseApplicationXML($Request);

        return $this->returnArray;
    }

    /**
     * @return mixed
     */
    private function generateCancelAppXML()
    {
        $xml = new SimpleXMLElement('<Envelope/>');

        $aCustomerInfo  = $this->aCustomerInfo;
        $aAgreementInfo  = $this->aAgreementInfo;;

        $soapBody     = $xml->addChild('Body');
        $CancelApp    = $soapBody->addChild('CancelAgreement');
        $NewCancelApp = $CancelApp->addChild('cancelAgreementData');

        $UserCredentials = $NewCancelApp->addChild('UserCredentials');
        $UserCredentials->addChild('LoginName', $this->apiLoginName);
        $UserCredentials->addChild('Password', $this->apiPassword);

        $Customer = $NewCancelApp->addChild('Customer');
        $Customer->addChild('Forename', $aCustomerInfo['Forename']);
        $Customer->addChild('Surname', $aCustomerInfo['Lastname']);
        $Customer->addChild('Email', $aCustomerInfo['EmailAddress']);

        $sml = $this->soapyfyXML($xml->asXML());

        return $sml;
    }

    /**
     * @param $xml_string
     */
    public function parseApplicationXML($xml_string)
    {
        $xml = simplexml_load_string($xml_string);
        $xml->registerXPathNamespace('envoy', 'http://www.barclays-partnerfinance.com/');

        $applicationResponse = $xml->xpath('//envoy:SubmitNewApplicationShortResult');

        // Application response is returned as a SOAP object
        $responseArray = json_decode(json_encode($applicationResponse), 1);

        $aParsedErrors = $this->parseErrors($responseArray);

        // Master return array set
        $this->returnArray['errors']            = $aParsedErrors;
        $this->returnArray['applicationToken']  = $responseArray[0]['Token'];
        $this->returnArray['ProposalID']        = $responseArray[0]['ProposalID'];
        $this->returnArray['xml']               = $xml_string;
    }

    /**
     * @param $responseArray
     * @return array
     */
    private function parseErrors($responseArray)
    {
        $aParsedErrors = array();
        if(isset($responseArray[0]['Errors'])){
            foreach($responseArray[0]['Errors']['ErrorDetails']['ErrorDetail'] as $key => $errors)
                // If/else to check for error codes, BPF returns different arrays depending on error
                if(isset($errors['@attributes'])){
                    // Don't display a particular error code (not needed)
                    if($errors['@attributes']['Code'] != 1)
                        $aParsedErrors[$key] = $errors['@attributes'];
                }elseif(isset($errors['Code'])){
                    $aParsedErrors[$key] = $errors;
                }
        }
        return $aParsedErrors;
    }

    /**
     *
     */
    private function setCustomerReferenceError()
    {
        $this->returnArray['errors'] = array(
            "errors" => array(
                "Message" => "Customer Reference Number Not set.",
                "Code"  => "999"
            )
        );
    }

} 