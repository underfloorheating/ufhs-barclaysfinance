Please note: it is strongly recommended you take a full backup of your website (including database) before installation of any Magento extension.

Installation:-

1. Upload all files, retaining the directory structure
2. Clear the Magento cache
3. Log out of the Magento backend, and log back in again

Configuration:-

1. Go to System -> Configuration and navigate to Sales -> Payment Methods
2. Expand the Barclays Partner Finance section
3. Enable the extension by setting "Enabled" to "Yes"
4. Configure the API Username, Password, Submit URL and Redirect URL (see details below)
5. Save the configuration

Api Username & Api Password: The API Username and Password will be provided by your Barclays Partner Finance representative.

Api Submit URL: This URL determines the location of the Barclays server that you will be sending information to. This can be changed to a live environment as the default URL is a testing enviroment.

User Redirect URL: This URL is the location where the user will be redirected to once finished with processing an order on your site. From this location the user will then fill in any extra details that are required for their application.
